## 基本概念

1. odoojs 是 js 代码库. 用在前端开发中.
2. 分为两部分. odoojs-rpc 和 odoojs-api
3. odoojs-rpc 管理所有的对 odoo 服务端的请求
4. odoojs-api 管理所有的前端自定义的 action, view, menu 以及 addons
5. odoojs-rpc 和 odoojs-api 已经发布为 npm 包.
6. 基于 antdv, 封装了一些组件. 适用于 pc 端开发.
7. 基于 uview-plus, 封装了一些组件. 适用于 uniapp 开发
8. 这些封装的组件, 有一点点方便处理 odoo 的功能. 是否可以封装为 ui-lib. 待议.
9. odoojs 架构, 这个词不严谨. 目前笼统的包括 上述内容以及官方 odoo

## odoojs-rpc

1. 对 axios 进行二次封装. 所有的请求, 封装为一个异步函数.
2. 对 服务端 odoo 模型方法的调用, 封装到 Model 类里面.
3. 为了管理编辑中的数据, 定义了 Editmodel 类.
4. 为了管理 one2many 字段的编辑, 定义了 EditX2m 类
5. 为了管理 one2many, many2many 字段, 定义了 One2many 及 Many2many 类
6. 为了使用 Model, Editmodel 方便, 定义 Environment 类
7. 入口 OdooJSRPC

### 登录

1. 获取 odoo 服务端数据之前, 需要获取 odoo 服务端的授权
2. 前端 调用 login 接口, 获取服务端授权
3. login 接口, 获取正确授权, 请求响应结果的头文件中携带 cokkie 信息.

### cookie 的使用

1. 前端若是使用浏览器访问, 登录请求响应之后, cokkie 自动写入浏览器中.
2. 后续, 前端发送其他请求时, 请求的头文件中自动携带 cokkie
3. 服务端通过 cokkie 验证授权. 若授权通过, 则返回的正确的响应结果.

#### cookie

1. 登录请求自动存储 cookie
2. 下次请求, 自动携带 cookie
3. 在 无 cookie 的环境中, 如微信小程序 调用 odoorpc:
4. 登录请求在 odoorpc 中 自动存储 session_id
5. 下次请求, 自动携带 session_id

### 非浏览器访问, 使用 cookie

1. 前端是移动 app, 微信小程序, 或者测试脚本时, 登录请求响应之后, 无法存储 cokkie
2. 此时可以模拟 浏览器请求. 使用 cokkie 访问 odoo. odoojs-rpc 在后续的版本中, 考虑支持该方案

### 非浏览器访问, 使用 session_id

1. 前端非浏览器访问, 登录请求之后, 获取到 cokkie.
2. odoojs-rpc 中, 自动从 cokkie 中获取 session_id, 并暂存.
3. 后续, 前端发送其他请求时, 请求的头文件中自动携带 session_id.
4. 服务端通过 session_id 验证授权. 若授权通过, 则返回的正确的响应结果.
5. odoojs-rpc 现在的版本中支持该功能.
6. 使用 node 运行测试脚本, 可以测试该功能.

### 微信小程序, 使用 odoojs-rpc

1. 微信小程序中使用 odoojs-rpc. 无法直接使用 axios
2. 需要修改调整 odoojs-rpc 的底层请求模块, 以支持 微信小程序
3. 该功能已测试过. 暂时尚未集成到 odoojs-rpc 现在的版本中.
4. 有需要该功能的, 可以直接和 odoojs 开发团队联系.

### 模型

1. odoojs 中的 Model. 只是一个函数集合. 封装了所有的对 服务端 odoo 模型方法的调用
2. 在本文中, 不特别说明. Model 指的是 odoojs 中的模型.
3. 而 服务端的 odoo 模型. 会特别指明是 服务端模型或者 odoo 模型.
4. 使用 rpc.env.model 函数, 获取 Model
5. 之后可以直接使用 Model 中的方法.

### Editmodel

#### 关于状态管理的基本逻辑

1. 前端项目. 在页面中仅仅是数据的直接呈现.
2. 前端页面中, 不能出现业务处理逻辑
3. 与业务相关的任何数据的处理, 都应该在服务端实现
4. 前端交互后, 应将交互结果, 提交给服务器, 由服务端处理.
5. 服务端处理后, 前端重新从服务端获取最新的数据, 并呈现.

#### odoo 的编辑页面的处理逻辑

1. odoo 编辑页面, 不符合上述关于状态管理的要求
2. 编辑页面, 首先读取数据. 此时前端页面中呈现编辑前的数据.
3. 将页面置为编辑状态. 该状态在 odoo 服务端无任何地方暂存.
4. 修改某一个字段的值. 修改后的结果在页面暂存.
5. 因 odoo 本身的管理逻辑. 当修改某一个字段的值之后, 触发服务端的 onchange
6. onchange 是一个请求. 返回值是其他字段的值有什么相应的变化.
7. onchange 的参数是当前页面编辑后的值. 返回结果是其他字段相应变化后的值.
8. onchange 的处理过程, 不符合前端页面无状态的要求.
9. 就是说, 要使用 onchange, 前端页面需要自己管理自己的状态. 服务端无地方暂存编辑的中间结果.

#### 编辑页面的处理过程

1. 前端页面, 查询获取数据, 显示.
2. 改为编辑状态. 前端页面暂存上述数据
3. 修改某一字段的值, 暂存.
4. 发送请求, 调用 onchange 函数. 参数是前端编辑后的数据
5. onchange 异步返回结果, 结果包括 其他字段相应的变化.
6. 将 onchange 返回结果, 与上述暂存结果, 合并.
7. 合并后的数据为 页面展示需要的数据
8. 上述过程中, onchange 前的数据暂存. 这就是一种状态.
9. 在异步 onchange 之后, 需要再次获取暂存的东西, 这意味着前端页面需要记忆自己的状态.
10. 这样的处理逻辑, 严重增加了前端页面的复杂性.
11. 很不幸, 使用 odoo, 使用 odoo 的 onchange. 上述情况无法避免.

#### odoojs 的 Editmodel 将编辑数据进行了封装, 以减少前端页面的处理逻辑

1. odoojs 仅仅是一个函数库, 无法改变上述处理逻辑
2. odoojs 的 Editmodel 可以将上述复杂的处理逻辑. 独自承担起来.
3. 从而让前端页面, 看起来, 像是只关心数据呈现. 不关心数据的业务逻辑

#### odoojs 的 Editmodel 的处理逻辑

1. 前端页面与 Editmodel 交互
2. 前端页面置为编辑状态时, 需要实例化一个 Editmodel
3. 前端页面将当前数据写入 Editmodel
4. 前端页面对数据的任何编辑和修改, 都更新进 Editmodel
5. Editmodel 自行管理 onchange. 前端页面 无需关心 onchange 的服务端请求
6. 前端页面随时 从 Editmodel 获取当前的所有数据. 并呈现.

## call_onchange

1. call_onchange(ids, values, field_name, field_onchange, kwargs = {})
2. 编辑时使用. 底层调用的是 onchange 方法
3. 对返回结果, 做了标准化处理

## Model

### 获取模型 执行模型方法

```
import { rpc } from '@/odoojs/index.js'

async function test_model() {
  const Model = rpc.env.model('res.users')
  console.log('Model', Model)

  const res = await Model.read_one(Model.env.uid, { fields: ['name', 'login']})
  console.log('one user', res)

  const res1 = await Model.read(Model.env.uid, {fields: ['name', 'login']})
  console.log('users', res1)

  const res2 = await Model.call_kw('read', [Model.env.uid], {fields: ['name', 'login']})
  console.log('users', res2)

  const res3 = await Model.call_kw('has_group', ['base.group_no_one'], {})
  console.log('has_group', res3)
}

```

1. 获取模型, 参数是模型名
2. 执行模型的方法
3. Model.read 是一个 官方 odoo 的通用方法
4. Model.read_one 是 在 read 方法基础上额外做了点加工
5. Model.call_kw 是万能方法
6. has_group 是 用户模型的专用方法, 只能通过 万能方法 调用
7. 当然, 对于 官方 odoo 已经完善的 模型的专用方法, 可以在 odoojs 中打补丁, 定义好

## 官方 odoo 的通用方法

1. call_kw(method, args, kwargs = {})
2. fields_get(allfields, kwargs = {})
3. web_search_read(kwargs = {})
4. search_read(kwargs = {})
5. name_search(kwargs = {})
6. name_get(ids)
7. read(ids, kwargs = {})
8. read_one(res_id, kwargs = {}) 非官方方法
9. create(vals, kws = {})
10. write(rid, vals, kws = {})
11. unlink(rid)
12. onchange(ids, values, field_name, field_onchange, kwargs = {})

## 说明

1. call_kw 万能方法
2. fields_get 获取元数据
3. web_search_read 返回 数据 及 数据总数
4. search_read 返回 数据
5. name_search 下拉框选项使用
6. name_get m2m 显示数据用
7. read 读数据. 返回数组
8. read_one 非官方方法. 是 对 read 返回值 处理为单条
