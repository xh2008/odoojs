## 使用 api 组织页面

## user

1. 使用 api.env.user 获取当前登录用户
2. 使用 api.env.uid 获取当前登录用户的 ID
3. 使用 api.web.session.session_info 获取当前登录用户的 session_info
4. 以上信息可以用于前端页面显示登录用户的信息

```
import api from '@/odoojs/index.js'
async function test_lang() {
  const user = api.env.user
  console.log('user', user)
  const uid = api.env.uid
  console.log('uid', uid)
  const session_info = api.web.session.session_info
  console.log('session_info', session_info)
}
```
