### 模型

1. odoojs 中的 Model. 是一个函数集合. 封装了所有的对 服务端 odoo 模型方法的调用
2. 通过 rpc.env.model('model_name') 获取一个模型
3. 之后可以使用模型方法

### odoojs 扩展的几个模型方法

1. load_metadata({ fields, context, callback })
2. load_web_data({domain, fields, offset, limit, order, context, callback})
3. load_data({domain, fields, offset, limit, order, context, callback})
4. load_data_one(res_id, {fields, context, callback})
5. load_metadata 方法获取模型中字段的属性
6. load_web_data, load_data, load_data_one 获取模型数据

### fields 参数说明

1. fields 参数, 指定获得哪些字段.
2. fields 可以是 Array 数组或 object 对象
3. fields 为 Array 数组时, 格式为:
   ['field_name', {name: 'relation_field_name', fields: [] or {} }, ...]
4. fields 为 Object 对象时, 格式为:
   {'field_name': null, 'relation_field_name': {fields: [] or {} }, ...}
5. field_name 为普通字段的字段名称.
6. relation_field_name 为 relation(many2one, one2many, many2many)类型字段的名称
7. relation 类型字段, 可以嵌套定义子模型的字段. 嵌套格式也是一个 fields 参数格式
8. relation 类型字段, 不需要嵌套时, 可以用普通字段的参数格式

### callback 参数说明

1. 因为这个几个方法, 由参数 fields 决定, 可能嵌套调用多次
2. callback 参数是一个函数, 该函数可以分多次返回 执行结果.
3. 若服务端 已安装 web_for_odoojs 模块,
   并且在 前端项目初始化 odoojs-rpc 中, 已配置 参数 run_by_server = true.
   则 callback 参数无意义

### 其他参数

1. domain 参数是过滤条件
2. order 参数是排序条件
3. offset, limit 参数, 控制分页
4. res_id. 数据记录 ID

### load_metadata 返回结果

1. 类同 odoo 的 fields_get. 但对 relation 类型字段有额外补充
2. relation 类型字段的返回结果中, 额外增加一个属性 metadata.
3. 内容为嵌套的子模型的 字段定义

### odoojs 数据集的标准化格式

#### 官方 odoo 的 jsonrpc 接口函数 search_read, read 返回结果

1. 结果为数组
2. 每个元素是一条记录的数据
3. one2many, many2many 类型字段 为空时, 数值为 []
4. 其他类型字段 为空时, 数值为 false
5. many2one 字段不为空时, 数值为 [id, display_name]
6. one2many, many2many 类型字段 不为空时, 数值为 [id1, id2, ...]

#### 官方 odoo 的 jsonrpc 接口函数 search_read, read 返回结果的不便之处

1. 以上数据格式在使用中, 存在以下不便:
2. 用 false 表示空值, 在前端代码中, 出现类型错误
3. many2one 字段 读写的格式不一致, 经常需要转换
4. one2many, many2many 的数据格式, 无法表示嵌套的字段数据

#### odoojs-rpc 的 load_data, load_web_data, load_data_one 的返回结果的标准化处理

1. many2one 类型字段, 数据格式转化为 {id, display_name} 支持嵌套读取额外字段
2. many2one 类型字段, 若为空值, 数值为 {id: null}
3. many2many 类型字段, 数据格式转化为 [{id, display_name, ...}] 支持嵌套读取额外字段
4. one2many 类型字段, 数据格式转化为 [{id, display_name, ...}] 支持嵌套读取额外字段
5. boolean 类型字段, 数值格式与官方相同
6. integer, float, monetary 若为空值, 标准化为 0 或 0.0
7. 其他类型的字段, 若无空值, 标准化为 null

### load_data 的返回结果

1. 类同 odoo 的 search_read.
2. 返回结果为数组 [{id, field1, field2, ...}]

### load_web_data 的返回结果

1. 类同 odoo 的 web_search_read.
2. 返回结果为数组 {length, records: [{id, field1, field2, ...}]}

### load_data_one 的返回结果

1. 返回结果为对象 {id, field1, field2, ...}

### tree 页面. 数据读取流程

1. 指定 model_name
2. 定义 fields
3. 执行 load_metadata, 返回 metadata
4. 根据 页面搜索选择, 设置 domain
5. 根据页面排序选择, 设置 order
6. 根据页面分页情况, 设置 offset, limit
7. 执行 web_load_data, 返回 length, records
8. 根据 length, offset, limit 设置页面分页参数
9. 根据 metadata, 定义 页面显示 table 的 columns
10. 将 records 作为 页面显示 table 的 datasource
11. 根据 metadata 中各字段的 type. 定义 table 的每个 cell 的渲染方法
12. table 的行点击事件, 获取 该行记录的 id, 供编辑, 删除, 和 查看 使用

```
import { rpc } from '@/odoojs/index.js'
const model_name = 'res.partner'
const fields = [
  'name',
  'email',
  'bank_ids': { fields: ['bank_bic', 'bank_id']}
]

async function test_load_data(){
  const Model = rpc.env.model(model_name)
  const metadata = await Model.load_metadata({fields})
  const domain = []
  const order = ''
  const offset = 0
  const limit = 0
  const {length, records} = await Model.web_load_data({fields})
}
```

### raed 页面. 数据读取流程

1. 页面已知 数据记录的 ID: res_id.
2. 定义 model_name, fields
3. 执行 load_metadata, 返回 metadata
4. 执行 load_data_one, 返回 record
5. 组织页面显示, 根据 metadata 中各字段的 type, 渲染不同类型字段的数据

```
import { rpc } from '@/odoojs/index.js'
const model_name = 'res.partner'
const fields = [
  'name',
  'email',
  'bank_ids': { fields: ['bank_bic', 'bank_id']}
]

async function test_load_data_one(){
  const res_id = id1
  const Model = rpc.env.model(model_name)
  const metadata = await Model.load_metadata({fields})
  const record = await Model.load_data_one(res_id, {fields})
}
```

### edit 页面. 数据处理流程

1. 与读取页面相同的方法, 读取数据 返回 record_only
2. 初始化 editmodel, 后续编辑时用
3. 初始化 fields_all, 页面渲染时用. 代替只读页面的 metadata 的作用
4. record 为页面数据, 包含编辑后的结果, 可直接用于显示
5. record_only 为编辑前的只读数据
6. values 编辑过的字段的数据
7. record_only, values 在处理 one2many 字段的数据显示时 使用
8. 编辑某字段后, 触发 change 事件, 调用 onChange 函数
9. onChange 函数中, 更新 record, values, fields_all
10. 页面根据 record, fields_all 渲染各字段的值
11. 编辑中的 many2many 和 one2many 类型字段的 tree 组件渲染方法. 另外专门描述

```
import { rpc } from '@/odoojs/index.js'
const model_name = 'res.partner'
const fields = [
  'name',
  'email',
  'bank_ids': { fields: ['bank_bic', 'bank_id']}
]

let editmodel = null
let metadata = {}
let fields_all = {}
let record_only = {}
let record = {}
let values = {}

async function load_data(){
  // call when page load
  const res_id = id1
  const Model = rpc.env.model(model_name)
  metadata = await Model.load_metadata({fields})
  record_only = await Model.load_data_one(res_id, {fields})
  editmodel = rpc.env.editmodel(model_name, {metadata})
  fields_all = editmodel.get_fields_all(fields)
  record = { ...record_only }
  record = editmodel.set_edit({ record, values })
}

async function onChange(fname, val) {
  // call after edit one field
  const res = await editmodel.onchange(fname, val)
  const { record_display, domain, values } = res
  record = { ...record_display }
  values = { ...values }
  fields_all = editmodel.get_fields_all(fields)
}

```

### 删除数据

1. tree 页面, 或者 read 页面. 获得 记录 ID: res_id
2. 执行 模型的删除方法. 在服务端删除数据
3. 前端 tree 页面. 刷新数据
4. 前端 read 页面. 跳转到其他页面.

```
import { rpc } from '@/odoojs/index.js'
const model_name = 'res.partner'

async function test_unlink(){
  const res_id = id1
  const Model = rpc.env.model(model_name)
  await Model.unlink(res_id)
}
```
