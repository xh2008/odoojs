## odoojs-demo-vue-ant-v1 介绍

1. odoojs-demo-vue-ant-v1
2. 该项目 是 odoojs 的 demo
3. 前端基于 vue3, vue-cli, antdv
4. 依赖于 odoojs-rpc, odoojs-api
5. 选择 ant-design-vue v4 作为 ui 库.
6. odoojs-rpc 是一个 javascript 库, 是前端访问 odoo 的接口.
7. odoojs-api 是一个 javascript 库, 是管理 action, menu, view 的工具.
8. 导入 odoo-rpc. "import rpc from 'odoojs-rpc'"
9. 导入 odoo-api. "import api from 'odoojs-api'"
10. 通过 odoojs-rpc 从 odoo 服务端获取所有的数据.
11. 通过 odoojs-api 管理所有的 menu, action, view
12. 自定义 所有的 menu, action, view. 需要 自行创建 addons_odoo

##

1. 实现了 官方 odoo 前端的所有功能.
2. 自动识别 odoo 服务端已安装的模块.
3. 根据已经安装模块, 自行处理相应的 menu, action, view.
4. 非 odoo 官方的自定义模块. 需要在 odoo 服务端安装.
5. 同时 使用 odoojs 提供的 addons 扩展功能, 定义相应的 menu, action, view.
6. 因此 odoojs-demo 适配 自定义模块的扩展.
7. 可以选择 ant-design-vue 之外的 ui 库.
8. 仅需替换 项目 中相应的 ui 组件即可.
9. 可以选择 vue3 之外的 其他前端架构.
10. 而保持 odoojs-rpc, odoojs-api, addons 不变.
11. 支持多语言. 仅需 增加相应语言的 addons 补丁包.
