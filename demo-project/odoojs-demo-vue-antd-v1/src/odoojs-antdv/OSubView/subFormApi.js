import api from '@/odoojs/index.js'
import { computed } from 'vue'

export function useSubForm(props) {
  const Relation = computed(() => {
    if (!props.node) {
      return undefined
    }
    return api.relation_node(props.node)
  })

  const formview = computed(() => {
    if (!Relation.value) {
      return undefined
    }
    return Relation.value.form
  })

  const sheet = computed(() => {
    if (!formview.value) {
      return {}
    }

    const { readonly, record = {}, values = {}, parentInfo } = props

    const editable = !readonly

    return formview.value.get_arch_sheet({
      parent_info: parentInfo,
      editable,
      record,
      values
    })
  })

  return { formview, sheet }
}
