import { computed } from 'vue'

export function useWidget(props, ctx = {}) {
  const { emit } = ctx
  const readonly = computed(() => {
    const readonly1 = props.node.readonly

    const { editable } = props.formInfo || {}
    return readonly1 || !editable
  })

  const fieldInfo = computed(() => {
    const { meta = {} } = props.node
    return meta
  })

  const value_display = computed(() => {
    const fname = props.node.name
    const { record = {} } = props.formInfo || {}
    const val = record[fname]
    return val
  })

  const value2 = computed({
    get() {
      return props.modelValue
    },
    set(value) {
      emit('update:modelValue', value)
    }
  })

  function onChange(value) {
    // console.log('onChange  ', value)
    emit('change', props.node.name, value)
  }

  return { fieldInfo, readonly, value_display, value2, onChange }
}
