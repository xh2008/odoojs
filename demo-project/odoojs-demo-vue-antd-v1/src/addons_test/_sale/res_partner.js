export default {
  view_partner_tree: {
    _odoo_model: 'ir.ui.view',
    model: 'res.partner',
    type: 'tree',
    buttons: { create: false, edit: true, delete: false },
    arch: {
      sheet: {
        display_name: { string: 'Name' },
        user_id: { widget: 'many2one_avatar_user' },
        property_product_pricelist: {}
      }
    }
  },

  view_partner_form: {
    _odoo_model: 'ir.ui.view',
    model: 'res.partner',
    type: 'form',
    buttons: { create: false, edit: true, delete: false },
    arch: {
      sheet: {
        _group: {
          _group_1: {
            name: { required: 1, readonly: 1 },
            vat: {}
          },

          _group_2: {
            user_id: { widget: 'many2one_avatar_user' },
            property_product_pricelist: {}
          }
        }
      }
    }
  },

  view_partner_person_form: {
    _odoo_model: 'ir.ui.view',
    model: 'res.partner',
    type: 'form',
    buttons: { create: false, edit: true, delete: false },
    arch: {
      sheet: {
        _group: {
          _group_1: {
            name: {
              required: 1
            },
            vat: {}
          },

          _group_2: {
            phone: { widget: 'phone' },
            mobile: { readonly: 1 },
            email: {}
          }
        }
      }
    }
  },

  action_contacts_company: {
    _odoo_model: 'ir.actions.act_window',
    name: '客户(公司)',
    type: 'ir.actions.act_window',
    res_model: 'res.partner',
    domain: [['is_company', '=', true]],
    context: { default_is_company: true },

    views: {
      tree: 'view_partner_tree',
      form: 'view_partner_form'
    }
  },

  action_contacts_person: {
    _odoo_model: 'ir.actions.act_window',
    name: '客户(个人)',
    type: 'ir.actions.act_window',
    res_model: 'res.partner',
    domain: [['is_company', '=', false]],
    context: { default_is_company: false },

    views: {
      tree: 'view_partner_tree',
      form: 'view_partner_person_form'
    }
  }
}
