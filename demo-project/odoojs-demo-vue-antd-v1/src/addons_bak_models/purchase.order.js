function ModelCreator(Model) {
  class ExtendModel extends Model {
    constructor(...args) {
      super(...args)
    }

    static call_button_after(name, action_info) {
      if (name === 'action_create_invoice') {
        return
      } else {
        alert(`todo, ${name}`)
        throw 'error'

        // return action_info
      }
    }

    static async onchange(...args) {
      const [ids, values, field_name, field_onchange, kwargs = {}] = args
      const res = super.onchange(...args)

      if (field_name === 'order_line') {
        const { value: valueold } = res
        const value = { ...valueold }
        delete value.order_line
        return { ...res, value }
      } else {
        return res
      }
    }
  }

  return ExtendModel
}

const AddonsModels = {
  'purchase.order': ModelCreator
}

export default AddonsModels
